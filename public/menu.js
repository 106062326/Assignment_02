var level = 1;
var volume = 0.6;
var is_mute = 0;
var menuState = {
  create: function() {
    // Add a background image
    //game.add.image(0, 0, 'background');
    // Display the name of the game
    
    this.bgm = game.add.audio('menu_bgm');
    this.bgm.loop = true;
    this.bgm.volume = volume;
    this.bgm.play();
    background = game.add.tileSprite(0, 0, game.cache.getImage('bgtile').width, game.cache.getImage('bgtile').height, 'bgtile');
    var nameLabel = game.add.text(game.width/2, 50, 'Raiden', { font: '50px Comic Sans MS', fill: '#ffe153' });
    var nameLabel2 = game.add.text(game.width/2, 90, '-animal version-', { font: '30px Comic Sans MS', fill: '#ffe153' });
    nameLabel.anchor.setTo(0.5, 0.5);
    nameLabel2.anchor.setTo(0.5, 0.5);
    //game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
    this.rule = game.add.sprite(game.width/2, -300, 'rule');
    game.add.tween(this.rule).to({y: 100}, 1500).easing(Phaser.Easing.Bounce.Out).start();
    this.rule.scale.setTo(0.8, 0.8);
    this.rule.anchor.setTo(0.5, 0.0);
    // Show the score at the center of the screen
    //var scoreLabel = game.add.text(game.width/2, game.height/2,
    //'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
    //scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    //var startLabel = game.add.text(game.width/2, game.height-80, 'press the number key to choose level', { font: '25px Arial', fill: '#ffffff' });
    //startLabel.anchor.setTo(0.5, 0.5);
    //game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    l1button = game.add.button(10, 300, 'btn_l1',this.l1_click, this);
    l2button = game.add.button(175, 300, 'btn_l2',this.l2_click, this);
    l3button = game.add.button(340, 300, 'btn_l3',this.l3_click, this);
    lbbutton = game.add.button(257.5, 430, 'btn_lb',this.lb_click, this);
    mpbutton = game.add.button(92.5, 430, 'btn_mp',this.mp_click, this);
    speaker_btn = game.add.button(225, 540, 'speaker', this.set_mute, this);
    plus_btn = game.add.button(155, 540, 'plus', this.volume_plus, this);
    minus_btn = game.add.button(295, 540, 'minus', this.volume_minus, this);
    speaker_btn.scale.setTo(0.1, 0.1);
    plus_btn.scale.setTo(0.1, 0.1);
    minus_btn.scale.setTo(0.1, 0.1);
    
    
  },
  set_mute: function(){
    if(is_mute == 0){
      is_mute = 1;
      
    }
    else{
      is_mute == 0;
    }
  },
  volume_plus: function(){
    if(volume< 1) volume += 0.1;
    if(volume>=1) volume = 1;
    //console.log(this.bgm.volume);
    this.bgm.volume = volume;
  },
  volume_minus: function(){
    if(volume > 0) volume -= 0.1;
    if(volume<=0) volume = 0;
    this.bgm.volume = volume;
    //console.log(this.bgm.volume);
  },
  l1_click: function(){
    this.bgm.stop();
    level = 1;
    game.state.start('play');
  },
  l2_click: function(){
    this.bgm.stop();
    level = 2;
    game.state.start('play');
  },
  l3_click: function(){
    this.bgm.stop();
    level = 3;
    game.state.start('play');
  },
  mp_click: function(){
    this.bgm.stop();
    level = 4;
    game.state.start('multi');
  },
  lb_click: function(){
    this.bgm.stop();
    game.state.start('LeaderBoard');
  },
  start: function() {
    // Start the actual game
    
    game.state.start('play');
  },
  
}; 