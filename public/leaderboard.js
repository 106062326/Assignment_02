var load_flag;
var LeaderBoardState = {
    create: function() {
      this.bgm = game.add.audio('menu_bgm');
      this.bgm.loop = true;
      this.bgm.volume = volume;
      this.bgm.play();
      background = game.add.tileSprite(0, 0, game.cache.getImage('bgtile').width, game.cache.getImage('bgtile').height, 'bgtile');
      var nameLabel = game.add.text(game.width/2, 50, 'Leader Board', { font: '50px Comic Sans MS', fill: '#24367D' });
      nameLabel.anchor.setTo(0.5, 0.5);
      var j =0;
      // Show the score at the center of the screen
      for(var i=0; i<4; i++)
      {
        if(i!=3)
          var levelLabel = game.add.text(125+(i%2)*250, 100+Math.floor(i/2) * 250, 'Level '+ (i+1), { font: '40px Comic Sans MS', fill: '#24367D' });     
        else
        var levelLabel = game.add.text(125+(i%2)*250, 100+Math.floor(i/2) * 250, 'Multi Player', { font: '40px Comic Sans MS', fill: '#24367D' });
        levelLabel.anchor.setTo(0.5, 0.5);
        var ScoreLabel;
        var pos_x, pos_y;
        firebase.database().ref('level'+ (i+1) +'/').orderByChild("score").limitToLast(3).once('value', function (snapshot) {
            snapshot.forEach(function (item) {
                //console.log(item.val().name + ":" + item.val().score);
                if(j>=6)
                {
                  pos_y = -(j%3)*40+ 150+250+80;
                  if(j>=9) pos_x = 125+250;
                  else pos_x = 125;
                } 
                else
                {
                  pos_y = -(j%3)*40+ 150+80;
                  if(j>=3) pos_x = 125+250;
                  else pos_x = 125;
                }  
                //console.log('x:'+pos_x +'y:'+pos_y+item.val().name + ":" + item.val().score);
                ScoreLabel = game.add.text(pos_x, pos_y, item.val().name + ":" + item.val().score, { font: '35px Comic Sans MS', fill: '#4f9d9d' });
                ScoreLabel.anchor.setTo(0.5, 0.5);
                j += 1;
                
            })
        });
      }
      // Explain how to start the game
      //var startLabel = game.add.text(game.width/2, game.height-80, 'press the number key to choose level', { font: '25px Arial', fill: '#ffffff' });
      //startLabel.anchor.setTo(0.5, 0.5);
      //game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
      // Create a new Phaser keyboard variable: the up arrow key
      // When pressed, call the 'start'
      var startLabel = game.add.text(game.width/2, game.height-50, 'press up key to return to menu', { font: '25px Comic Sans MS', fill: '#24367D	' });
      startLabel.anchor.setTo(0.5, 0.5);
      //game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
      // Create a new Phaser keyboard variable: the up arrow key
      // When pressed, call the 'start'
    
      var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
      upKey.onDown.add(this.menu, this); 
      
    },
  
    
    menu: function() {
      // Start the actual game
      this.bgm.stop();
      game.state.start('menu');
    },
    
  }; 