var loadState = {
  preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, game.height/2, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);
    
    // Load all game assets
    game.load.image('rule', 'assets/rule.png');
    game.load.spritesheet('player', 'assets/boy.png', 355, 455);
    game.load.spritesheet('player2', 'assets/boy2.png', 355, 455);
    game.load.image('stone', 'assets/stone.png');
    game.load.image('star','assets/star.png' );
    game.load.image('star2','assets/star2.png' );
    game.load.image('diamond','assets/diamond.png' );
    game.load.image('heart','assets/heart.png' );
    game.load.image('cherry','assets/cherry.png' );
    game.load.image('bone','assets/bone.png' );

    game.load.spritesheet('rabbit', 'assets/rabbit.png', 65, 100);
    game.load.spritesheet('goat', 'assets/goat.png', 77, 91);
    game.load.spritesheet('cat', 'assets/cat.png', 65, 85);
    game.load.spritesheet('dog', 'assets/dog.png', 82, 80);
    game.load.image('wallV', 'assets/wallVertical.png');
    game.load.image('wallH', 'assets/wallHorizontal.png');
    // Load a new asset that we will use in the menu state
    

    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('spark', 'assets/spark.png');
    game.load.image('bgtile', 'assets/bg.jpg');
    game.load.image('btn_l1', 'assets/btn_l1.png');
    game.load.image('btn_l2', 'assets/btn_l2.png');
    game.load.image('btn_l3', 'assets/btn_l3.png');
    game.load.image('btn_lb', 'assets/btn_lb.png');
    game.load.image('btn_mp', 'assets/btn_mp.png');
    game.load.image('speaker', 'assets/speaker.png');
    game.load.image('plus', 'assets/plus.png');
    game.load.image('minus', 'assets/minus.png');
    game.load.image('pause', 'assets/pause.png');
    

    game.load.audio('hurt', 'assets/hurt.mp3');
    game.load.audio('play_bgm', 'assets/play_bgm.mp3');
    game.load.audio('menu_bgm', 'assets/menu_bgm.mp3');
    game.load.audio('attack', 'assets/pick.mp3');
  },
  create: function() {
    // Go to the menu state
    game.state.start('menu');
  }
}; 