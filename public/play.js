var bullets;
var bgtile;
var fireRate = 100;
var nextFire = 0;
var next_skill= 5000;
var skillRate = 5000;
var last_hurt;
var skill_num;
var is_died = 0;
var is_pause = 0;
var strong_bullet = 0;
var currentExplosion = 0;

var playState = {
  create: function() {

    next_skill = game.time.now + next_skill;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    bgtile = game.add.tileSprite(0, 0, 500, game.cache.getImage('bgtile').height, 'bgtile');
    is_died = 0;
    //sound
    this.hurt_sound = game.add.audio('hurt');
    this.attack_sound = game.add.audio('attack');
    this.hurt_sound.volume = volume;
    this.attack_sound.volume = volume;
    this.bgm = game.add.audio('play_bgm');
    this.bgm.loop = true;
    this.bgm.volume = volume;
    this.bgm.play();

    // PLAYER
    this.player = game.add.sprite(game.width/2, game.height*3/4, 'player');
    this.player.scale.setTo(0.1, 0.1);
    game.physics.arcade.enable(this.player);
    //this.player.body.gravity.y = 500;
    this.player.animations.add('rightwalk', [8, 11], 8, true);
    this.player.animations.add('leftwalk', [4, 7], 8, true);
    this.player.animations.add('forwardwalk', [1, 3], 8, true);

    //Boss
    this.boss = game.add.sprite(game.width/2, 100, 'dog');
    game.physics.arcade.enable(this.boss);
    this.boss.body.bounce.x = 1;
    this.boss.anchor.setTo(0.5, 0.5);
    this.boss.kill();
    // WALL
    this.walls = game.add.group(); 
    this.walls.enableBody = true;

    this.leftwall = game.add.sprite(-19, 0, 'wallV', 0, this.walls); 
    this.leftwall.scale.setTo(1, 3);
    this.rightwall = game.add.sprite(499, 0, 'wallV', 0, this.walls);
    this.rightwall.scale.setTo(1, 3);
    this.bottomfloor = game.add.sprite(0, 590, 'wallH', 0, this.walls);
    this.bottomfloor.scale.setTo(3, 1);
    this.topfloor = game.add.sprite(0, -20, 'wallH', 0, this.walls);
    this.topfloor.scale.setTo(3, 1);

    this.walls.setAll('body.immovable', true);

    // INPUT
    this.cursor = game.input.keyboard.createCursorKeys();
    this.space = game.input.keyboard.addKeys(Phaser.Keyboard.SPACEBAR);


    // SCORE & LIFE
    game.global.score = 0;
    this.scoreLabel = game.add.text(game.width/2, 30, 'score: 0', { font: '24px Arial', fill: '#000000' });
    this.scoreLabel.anchor.setTo(0.5, 0.0);

    game.global.playerskill = 3;
    this.skillLabel = game.add.text(30, 50, 'Skill: 3', { font: '18px Arial', fill: '#0072e3' })

    game.global.playerLife = 3;
    this.lifeLabel = game.add.text(30, 30, 'Life: 3', { font: '18px Arial', fill: '#0072e3' })

    pause_btn = game.add.button(440, 10, 'pause', this.pause, this);


    // ENEMY
    this.rabbits = game.add.group();
    this.rabbits.enableBody = true;
    // Create 10 enemies in the group.
    this.rabbits.createMultiple(10, 'rabbit');
   

    this.goats = game.add.group();
    this.goats.enableBody = true;
    this.goats.createMultiple(10, 'goat');
    

    this.cats = game.add.group();
    this.cats.enableBody = true;
    this.cats.createMultiple(10, 'cat');

    //player bullet
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.createMultiple(60, 'star');
    bullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    bullets.callAll('scale.setTo', 'scale', 0.2, 0.2);
    bullets.setAll('checkWorldBounds', true);
    bullets.setAll('outOfBoundskill', true);

    sbullets = game.add.group();
    sbullets.enableBody = true;
    sbullets.createMultiple(10, 'star2');
    sbullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    sbullets.setAll('checkWorldBounds', true);
    sbullets.setAll('outOfBoundskill', true);

    //enemybullet
    ebullets = game.add.group();
    ebullets.enableBody = true;
    ebullets.createMultiple(150, 'stone');
    ebullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    ebullets.callAll('scale.setTo', 'scale', 0.2, 0.2);
    ebullets.setAll('checkWorldBounds', true);
    ebullets.setAll('outOfBoundskill', true);

    //catbullet
    cbullets = game.add.group();
    cbullets.enableBody = true;
    cbullets.createMultiple(150, 'stone');
    cbullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    cbullets.callAll('scale.setTo', 'scale', 0.2, 0.2);
    cbullets.setAll('checkWorldBounds', true);
    cbullets.setAll('outOfBoundskill', true);

    //bossbullet
    this.bbullets = game.add.group();
    this.bbullets.enableBody = true;
    this.bbullets.createMultiple(60, 'bone');
    this.bbullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    this.bbullets.callAll('scale.setTo', 'scale', 0.8, 0.8);
    this.bbullets.setAll('checkWorldBounds', true);
    this.bbullets.setAll('outOfBoundskill', true);

    //item
    this.hearts = game.add.group();
    this.hearts.enableBody = true;
    this.hearts.createMultiple(3, 'heart');
    this.hearts.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    this.hearts.callAll('scale.setTo', 'scale', 0.4, 0.4);
    this.cherrys = game.add.group();
    this.cherrys.enableBody = true;
    this.cherrys.createMultiple(3, 'cherry');
    this.cherrys.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    this.cherrys.callAll('scale.setTo', 'scale', 1.5, 1.5);
    game.time.events.loop(16000, this.addItem, this);
    //level
    if(level == 1){
      game.time.events.loop(3000, this.addRabbit, this);
      game.time.events.loop(10000, this.addGoat, this);
      game.time.events.loop(1600, this.rabbit_attack, this);
      game.time.events.loop(2100, this.goat_attack, this);
    }
    else if(level == 2){
      game.time.events.loop(2000, this.addRabbit, this);
      game.time.events.loop(10000, this.addGoat, this);
      game.time.events.loop(13000, this.addCat, this);
      game.time.events.loop(1100, this.rabbit_attack, this);
      game.time.events.loop(2000, this.goat_attack, this);
      game.time.events.loop(3100, this.cat_attack, this);
    }
    else if(level == 3){
      game.time.events.loop(3000, this.addRabbit, this);
      game.time.events.loop(10000, this.addGoat, this);
      game.time.events.loop(13000, this.addCat, this);
      game.time.events.loop(700, this.rabbit_attack, this);
      game.time.events.loop(1100, this.goat_attack, this);
      game.time.events.loop(2000, this.cat_attack, this);
      game.time.events.add(34000, this.addBoss, this);
    }
    

    // PARTICLE
    this.eb_emitter = game.add.emitter(0, 0, 15);
    this.eb_emitter.makeParticles('pixel');
    this.eb_emitter.setYSpeed(-150, 150);
    this.eb_emitter.setXSpeed(-150, 150);
    this.eb_emitter.setScale(2, 0, 2, 0, 500);
    this.eb_emitter.gravity = 0;

    
    
    this.pb_emitters = [];
    
    var emitter;
    for (var e = 0; e < 10; e++){
      emitter = game.add.emitter(0, 0, 5);
      emitter.makeParticles('spark');
      emitter.setYSpeed(-150, 0);
      emitter.setXSpeed(-150, 150);
      emitter.setScale(2, 0, 2, 0, 500);
      emitter.gravity = 0;
      /* ... your emitter setup here ... */  
      this.pb_emitters.push(emitter);
    }

    
  },
  update: function() {
    bgtile.tilePosition.y += 1;
    // COLLISION
    game.physics.arcade.collide(this.rabbits, this.rightwall);
    game.physics.arcade.collide(this.rabbits, this.leftwall);
    game.physics.arcade.collide(this.goats, this.rightwall);
    game.physics.arcade.collide(this.goats, this.leftwall);
    game.physics.arcade.collide(this.cats, this.rightwall);
    game.physics.arcade.collide(this.cats, this.leftwall);
    game.physics.arcade.collide(this.boss, this.rightwall);
    game.physics.arcade.collide(this.boss, this.leftwall);  
    game.physics.arcade.collide(this.boss, this.bottomfloor);
    game.physics.arcade.collide(this.boss, this.topfloor);  
    game.physics.arcade.collide(this.walls, this.walls);
    game.physics.arcade.collide(this.player, this.walls);
    game.physics.arcade.overlap(this.player,this.rabbits, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player,this.goats, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player,this.cats, this.playerDie, null, this);

    game.physics.arcade.overlap(sbullets, this.rabbits, this.enemyDie, null, this);
    game.physics.arcade.overlap(sbullets, this.goats, this.enemyDie, null, this);
    game.physics.arcade.overlap(sbullets, this.cats, this.enemyDie, null, this);
    game.physics.arcade.overlap(sbullets, ebullets, this.enemyDie, null, this);
    game.physics.arcade.overlap(sbullets, cbullets, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets, this.rabbits, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets, this.goats, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets, this.cats, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets, this.boss, this.BossDie, null, this);
    game.physics.arcade.overlap(sbullets, this.boss, this.BossDie, null, this);
    game.physics.arcade.overlap(this.player, this.bbullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player, ebullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player, cbullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player, this.hearts, this.takeHeart, null, this);
    game.physics.arcade.overlap(this.player, this.cherrys, this.takeCherry, null, this);
    this.movePlayer();
    if (!this.player.inWorld) { this.playerDie();}
    if(next_skill > game.time.now)
    {
      this.skillLabel.addColor("#ffffff", 0);
    }
    else this.skillLabel.addColor("#0072e3", 0);
    
  
  }, // No changes
  movePlayer: function() {

    // LEFT RIGHT
    if (this.cursor.left.isDown) {
      this.player.body.velocity.x = -200;
      this.player.animations.play('leftwalk'); 
    }
    else if (this.cursor.right.isDown) {
      this.player.body.velocity.x = 200;
      this.player.animations.play('rightwalk'); 
    }
    else if (this.cursor.up.isDown) {
      this.player.body.velocity.y = -200;
      this.player.animations.play('forwardwalk'); 
    } 
    else if(this.cursor.down.isDown){
      this.player.body.velocity.y = 200;
      this.player.animations.play('forwardwalk'); 
    }
    else {
      this.player.body.velocity.x = 0;
      this.player.body.velocity.y = 0;
      this.player.animations.play('forwardwalk');
     
    } 
      
    // shoot
    if(game.input.keyboard.isDown(Phaser.KeyCode.ENTER)){
      fire(this.player);
      //console.log("space!");
    }
    //skill
    if(game.input.keyboard.isDown(Phaser.Keyboard.ONE))
    {
      this.use_skill();
    }
  },
  takeHeart: function(player, heart) {
    // SCORE
    if(game.global.playerLife<3)
      game.global.playerLife += 1;
    this.lifeLabel.text = 'life: ' + game.global.playerLife;
    heart.kill();
   

    // TWEEN
    game.add.tween(this.player.scale).to({x: 0.15, y: 0.15}, 100).yoyo(true).start();
  },
  takeCherry: function(player, cherry) {
    // SCORE
    strong_bullet = 1;
    cherry.kill();
    // TWEEN
    game.add.tween(this.player.scale).to({x: 0.15, y: 0.15}, 100).yoyo(true).start();
    game.time.events.add(Phaser.Timer.SECOND*5, this.not_strong_bullet, this);
  },
  not_strong_bullet:function(){
    strong_bullet = 0;
  },
  addItem: function(){
    var which = game.rnd.pick(['heart', 'cherry']);
    var posx = Math.floor((Math.random() * 350)+75);
    var posy = game.rnd.pick([350,360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500]);
    if(which == 'heart')
    {
      var heart = this.hearts.getFirstDead();
      if(!heart) return;
      heart.reset(posx, posy);
    }
    else if(which == 'cherry')
    {
      var cherry = this.cherrys.getFirstDead();
      if(!cherry) return;
      //cherry.scale.setTo(1.5, 1.5);
      cherry.reset(posx, posy);
    }
  },
  addBoss: function(){
    this.boss.reset(game.width/2, 100);
    this.boss.health = 30;
    //console.log("Bosshealth:"+this.boss.health);
    this.boss.animations.add('walk', [1, 3], 8, true);
    this.Boss_move();
  },
  addRabbit: function() {
    
    var rabbit = this.rabbits.getFirstDead();
    var posx = game.rnd.pick([100, 150, 200, 250, 300, 350, 400]);
    rabbit.scale.setTo(0.4, 0.4);
    if (!rabbit) return;
    rabbit.anchor.setTo(0.5, 1);
    rabbit.reset(posx, 50);
    rabbit.body.velocity.y = 40;
    rabbit.body.velocity.x = 50 * game.rnd.pick([-1, 1]) * (Math.random() +1);
    rabbit.body.bounce.x = 1;
    rabbit.checkWorldBounds = true;
    rabbit.outOfBoundsKill = true;
    rabbit.animations.add('walk', [1, 3], 8, true);
    rabbit.animations.play('walk'); 
    rabbit.health = 1;
  },
  addGoat: function() {
    
    var goat = this.goats.getFirstDead();
    var posx = game.rnd.pick([100, 150, 200, 250, 300, 350, 400, 450])-30;
    goat.scale.setTo(0.4, 0.4);
    if (!goat) return;
    goat.alpha = 1.0;
    goat.anchor.setTo(0.5, 1);
    goat.reset(posx, 50);
    goat.body.velocity.y = 40;
    goat.body.velocity.x = 50 * game.rnd.pick([-1, 1]) * (Math.random() +1);
    goat.body.bounce.x = 1;
    goat.checkWorldBounds = true;
    goat.outOfBoundsKill = true;
    goat.animations.add('walk', [1, 3], 8, true);
    goat.animations.play('walk'); 
    goat.setHealth(2);
    //console.log("Goathealth:"+goat.health);
  },
  addCat: function() {
    
    var cat = this.cats.getFirstDead();
    var posx = game.rnd.pick([50, 450])
    cat.scale.setTo(0.4, 0.4);
    
    if (!cat) return;
    cat.alpha = 1.0;
    cat.anchor.setTo(0.5, 1);
    cat.reset(posx, 200);
    cat.body.velocity.y = 20;
    cat.body.velocity.x = 50 * game.rnd.pick([-1, 1]) * (Math.random() +1);
    cat.body.bounce.x = 1;
    cat.checkWorldBounds = true;
    cat.outOfBoundsKill = true;
    cat.animations.add('walk', [1, 3], 8, true);
    cat.animations.play('walk'); 
    cat.setHealth(2);
  },
  playerDie: function(player, enemy) {
    if(last_hurt == enemy) return;
    this.hurt_sound.play();
    last_hurt = enemy;
    enemy.kill();
    //console.log("hurt");
    // PARTICLE SYSTEM
    
    this.eb_emitter.kill();
    this.eb_emitter.x = this.player.x;
    this.eb_emitter.y = this.player.y;
    this.eb_emitter.start(true, 800, null, 15);

    // CAMERA EFFECT
    //game.camera.flash(0xffffff, 300);
    game.camera.shake(0.02, 300);

    game.global.playerLife -= 1;
    this.lifeLabel.text = 'Life: ' + game.global.playerLife;
    if(game.global.playerLife <= 0)
    {
      is_died = 1;
      this.player.kill();
      game.time.events.add(1000, function() {
        this.bgm.stop();
        game.state.start('over');
  
      }, this);
    }
   
  },
  enemyDie: function(bullet, enemy){
    this.attack_sound.play();
    currentExplosion = (currentExplosion + 1) % 10;
    var emitter = this.pb_emitters[currentExplosion];
    emitter.x = enemy.x;
    emitter.y = enemy.y;
    emitter.start(true, 500, null, 15);
    enemy.health -=1;
    //console.log("enemydie");
    if(enemy.health<=0)
      enemy.kill();
    else
      enemy.alpha = enemy.health/3;

    if(strong_bullet == 0)
      bullet.kill();
    game.global.score += 5;
    this.scoreLabel.text = 'score: ' + game.global.score;
  },
  BossDie: function(enemy,bullet){
    this.attack_sound.play();
    currentExplosion = (currentExplosion + 1) % 10;
    var emitter = this.pb_emitters[currentExplosion];
    emitter.x = bullet.x;
    emitter.y = bullet.y;
    emitter.start(true, 500, null, 15);
    
    
    this.boss.health -= 1;
    if(this.boss.health <= 0)
    {
      this.boss.kill();
      game.global.score += 50;
      this.scoreLabel.text = 'score: ' + game.global.score;
      this.goats.forEachExists(function(enemy){enemy.kill();game.global.score += 5;});
      this.cats.forEachExists(function(enemy){enemy.kill();game.global.score += 5;});
      this.rabbits.forEachExists(function(enemy){enemy.kill();game.global.score += 5;});
      this.scoreLabel.text = 'score: ' + game.global.score;

      game.time.events.add(1000, function() {
        this.bgm.stop();
        game.state.start('over');
  
      }, this);
    }
    if(strong_bullet == 0)
      bullet.kill();
    
      
  },
  rabbit_attack: function(){
    this.rabbits.forEachExists(function(enemy){
      
        var ebullet = this.ebullets.getFirstDead();
        if(!ebullet) return;
        ebullet.reset(enemy.x+ 17.75, enemy.y);
        //ebullet.body.velocity.y = enemy.body.velocity.y*5.0;
        ebullet.body.velocity.x = enemy.body.velocity.x*2.0;
        ebullet.checkWorldBounds = true;
        ebullet.outOfBoundsKill = true;
        ebullet.body.gravity.y = 500;
        
    })
  },
  goat_attack: function(){
    this.goats.forEachExists(function(enemy){
        var attack_radius = 5;
        if(enemy.y >=250 && enemy.body.velocity.y != 0)
        {
          enemy.body.velocity.x = 0;
          enemy.body.velocity.y = 0;
          enemy.animations.stop();
          enemy.frame = 0
        }
        for(var i=0; i<6; i++)
        {
          var degree = i*(360/6)*Math.PI /180.0;
          var ebullet = this.ebullets.getFirstDead();
          if(!ebullet) return;
          ebullet.reset(enemy.x+ attack_radius * Math.cos(degree), enemy.y + attack_radius * Math.sin(degree));
          ebullet.body.velocity.y =  Math.sin(degree)*90.0;
          ebullet.body.velocity.x = Math.cos(degree)*90.0;
          ebullet.checkWorldBounds = true;
          ebullet.outOfBoundsKill = true;
          ebullet.body.gravity.y = 0;
        }
    })
  },
  cat_attack: function(){
    this.cats.forEachExists(function(enemy){
        if(enemy.y > 400 || enemy.y < 100)
        {
          //console.log(enemy.y);
          enemy.body.velocity.y *= -1;
        }
        for(var i=-1; i<2; i++)
        {
          var cbullet = this.cbullets.getFirstDead();
          if(!cbullet) return;
          cbullet.reset(enemy.x + 13, enemy.y +5);
          cbullet.body.velocity.y =  70.0;
          cbullet.body.velocity.x = i*50.0 + enemy.body.velocity.x;
          cbullet.checkWorldBounds = true;
          cbullet.outOfBoundsKill = true;
          cbullet.body.gravity.y = 0;
        }
    })
  },
  Boss_move: function(){
    this.boss.animations.play('walk');
    var vx = (this.player.x - this.boss.x)/Math.abs(this.player.x - this.boss.x) * (Math.random()+1)*40;
    var vy = (this.player.y - this.boss.y)/Math.abs(this.player.y- this.boss.y) * (Math.random()+1)*40;
    this.boss.body.velocity.x = vx;
    this.boss.body.velocity.y = vy;
    game.time.events.add(1500, this.Boss_attack, this);

  },
  Boss_attack: function(){
    if(this.boss.health<=0) return;
    var attack_mode = game.rnd.pick(['circle', 'shotgun']);
    if(attack_mode == 'circle')
    {
      var attack_radius = 10;
      for(var i=0; i<12; i++)
      {
        var degree = i*(360/12)*Math.PI /180.0;
        var bbullet = this.bbullets.getFirstDead();
        if(!bbullet) return;
        bbullet.reset(this.boss.x+ attack_radius * Math.cos(degree), this.boss.y + attack_radius * Math.sin(degree));
        bbullet.body.velocity.y =  Math.sin(degree)*150.0;
        bbullet.body.velocity.x = Math.cos(degree)*150.0;
        bbullet.checkWorldBounds = true;
        bbullet.outOfBoundsKill = true;
        bbullet.body.gravity.y = 0;
      }
    }
    else if(attack_mode == 'shotgun')
    {
      for(var i=-3; i<4; i++)
      {
        var bbullet = this.bbullets.getFirstDead();
        if(!bbullet) return;
        bbullet.reset(this.boss.x + 13, this.boss.y +5+ 20);
        bbullet.body.velocity.y =  this.boss.body.velocity.y/Math.abs(this.boss.body.velocity.y)*150.0;
        bbullet.body.velocity.x = i*70 + this.boss.body.velocity.x;
        bbullet.checkWorldBounds = true;
        bbullet.outOfBoundsKill = true;
        bbullet.body.gravity.y = 0;
      }
    }
    game.time.events.add(Phaser.Timer.SECOND*3, this.Boss_move, this);

  },
  use_skill: function(){
    if(game.global.playerskill <= 0 || game.time.now < next_skill) return;
    else
    {
    next_skill = game.time.now + skillRate;
      game.global.playerskill -= 1;
      this.skillLabel.text = 'Skill: ' + game.global.playerskill;
    }
    for(var i = 0; i < 2; i++)
    {
      for(var j=0; j<8; j++)
      {
        var bullet = bullets.getFirstDead();
        if(!bullet) return;
        bullet.scale.setTo(0.5, 0.5);
        bullet.reset(70 + j*50, this.player.y+ 10 + 40*i);
        bullet.body.velocity.y = -600;
       // console.log("shoot!");
        bullet.outOfBoundsKill = true;  
      }
        
    }
  },
  pause: function(){
    if(is_pause == 0)
    {
      is_pause = 1;
      this.game.paused = true;
    }
    else{
      is_pause = 0;
      this.game.paused = false;
    }
  }
  
};
function fire(player) {
  
  if (game.time.now > nextFire && bullets.countDead() > 0)
  {
      nextFire = game.time.now + fireRate;
      
      //console.log("shoot!");
        
      if(strong_bullet == 1){
        var bullet = sbullets.getFirstDead();
        if(!bullet) return;
        
        bullet.reset(player.x+ 17.75, player.y+10);
        bullet.body.velocity.y = -500;
      }
      else{
        var bullet = bullets.getFirstDead();
        if(!bullet) return;
        bullet.scale.setTo(0.3, 0.3);
        
        bullet.reset(player.x+ 17.75, player.y);
        bullet.body.velocity.y = -400;
      }  
      bullet.outOfBoundsKill = true; 
  }
};

