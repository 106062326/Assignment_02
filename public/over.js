var player='';
var overState = {
    create: function() {
      
      this.bgm = game.add.audio('menu_bgm');
      this.bgm.loop = true;
      this.bgm.volume = volume;
      this.bgm.play();
      background = game.add.tileSprite(0, 0, game.cache.getImage('bgtile').width, game.cache.getImage('bgtile').height, 'bgtile');
      var nameLabel = game.add.text(game.width/2, -50, 'Game over', { font: '50px Comic Sans MS', fill: '#24367D' });
      nameLabel.anchor.setTo(0.5, 0.5);
      game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
      // Show the score at the center of the screen
      var scoreLabel = game.add.text(game.width/2, game.height/2,
      'score: ' + game.global.score, { font: '40px Comic Sans MS', fill: '#ffe153' });
      scoreLabel.anchor.setTo(0.5, 0.5);
      // Explain how to start the game
      var startLabel = game.add.text(game.width/2, game.height-80, 'press up key to return to menu', { font: '30px Comic Sans MS', fill: '#24367D' });
      startLabel.anchor.setTo(0.5, 0.5);
      game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
      // Create a new Phaser keyboard variable: the up arrow key
      // When pressed, call the 'start'
    
      var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
      upKey.onDown.add(this.push_score, this); 
      player = prompt("Please enter your name", "name");
      
    },
    push_score: function(){
      firebase.database().ref('level'+ level +'/').push({
        name: player,
        score: game.global.score
      });
      this.menu();
    },
    menu: function() {
      // Start the actual game
      this.bgm.stop();
      game.state.start('menu');
    },
    
  }; 