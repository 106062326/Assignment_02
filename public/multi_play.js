var bullets;
var bgtile;
var p1_fireRate = 100;
var p2_fireRate = 500;
var p1_nextFire = 0;
var p2_nextFire = 0;
var p1_next_skill= 5000;
var p1_skillRate = 5000;
var p2_next_skill= 5000;
var p2_skillRate = 5000;
var last_hurt;
var p1_skill_num;
var p2_skill_num;
var p1_is_died = 0;
var p2_is_died = 0;
var is_pause = 0;
var currentExplosion = 0;
var MultiplayState = {
  create: function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    bgtile = game.add.tileSprite(0, 0, 500, game.cache.getImage('bgtile').height, 'bgtile');
    is_died = 0;
    p1_next_skill = game.time.now + p1_skillRate;
    p2_next_skill = game.time.now + p2_skillRate;
    //sound
    this.hurt_sound = game.add.audio('hurt');
    this.attack_sound = game.add.audio('attack');
    this.hurt_sound.volume = volume;
    this.attack_sound.volume = volume;
    this.bgm = game.add.audio('play_bgm');
    this.bgm.loop = true;
    this.bgm.volume = volume;
    this.bgm.play();
    // PLAYER
    
    this.player1 = game.add.sprite(game.width*3/4, game.height*3/4, 'player');
    this.player1.scale.setTo(0.1, 0.1);
    game.physics.arcade.enable(this.player1);
    //this.player.body.gravity.y = 500;
    this.player1.animations.add('rightwalk', [8, 11], 8, true);
    this.player1.animations.add('leftwalk', [4, 7], 8, true);
    this.player1.animations.add('forwardwalk', [1, 3], 8, true);

    this.player2 = game.add.sprite(game.width/4, game.height*3/4, 'player2');
    this.player2.scale.setTo(0.1, 0.1);
    game.physics.arcade.enable(this.player2);
    //this.player.body.gravity.y = 500;
    this.player2.animations.add('rightwalk', [8, 11], 8, true);
    this.player2.animations.add('leftwalk', [4, 7], 8, true);
    this.player2.animations.add('forwardwalk', [1, 3], 8, true);

    // WALL
    this.walls = game.add.group(); 
    this.walls.enableBody = true;

    this.leftwall = game.add.sprite(-19, 0, 'wallV', 0, this.walls); 
    this.leftwall.scale.setTo(1, 3);
    this.rightwall = game.add.sprite(499, 0, 'wallV', 0, this.walls);
    this.rightwall.scale.setTo(1, 3);
    this.bottomfloor = game.add.sprite(0, 590, 'wallH', 0, this.walls);
    this.bottomfloor.scale.setTo(3, 1);
    this.topfloor = game.add.sprite(0, -20, 'wallH', 0, this.walls);
    this.topfloor.scale.setTo(3, 1);

    this.walls.setAll('body.immovable', true);

    // INPUT
    this.cursor = game.input.keyboard.createCursorKeys();
    this.space = game.input.keyboard.addKeys(Phaser.Keyboard.SPACEBAR);


    // SCORE & LIFE
    game.global.score = 0;
    this.scoreLabel = game.add.text(game.width/2, 30, 'score: 0', { font: '24px Arial', fill: '#000000' });
    this.scoreLabel.anchor.setTo(0.5, 0.0);

    game.global.player1Life = 3;
    this.p1_lifeLabel = game.add.text(380, 30, 'life: 3', { font: '18px Arial', fill: '#0072e3' })

    game.global.player1skill = 3;
    this.p1_skillLabel = game.add.text(380, 50, 'Skill: 3', { font: '18px Arial', fill: '#0072e3' })

    game.global.player2Life = 3;
    this.p2_lifeLabel = game.add.text(30, 30, 'life: 3', { font: '18px Arial', fill: '#28ff28' })

    game.global.player2skill = 3;
    this.p2_skillLabel = game.add.text(30, 50, 'Skill: 3', { font: '18px Arial', fill: '#28ff28' })

    pause_btn = game.add.button(440, 10, 'pause', this.pause, this);



    // ENEMY
    this.rabbits = game.add.group();
    this.rabbits.enableBody = true;
    // Create 10 enemies in the group.
    this.rabbits.createMultiple(10, 'rabbit');
   

    this.goats = game.add.group();
    this.goats.enableBody = true;
    this.goats.createMultiple(10, 'goat');
    

    this.cats = game.add.group();
    this.cats.enableBody = true;
    this.cats.createMultiple(10, 'cat');
    
    //item
    this.hearts = game.add.group();
    this.hearts.enableBody = true;
    this.hearts.createMultiple(3, 'heart');
    this.hearts.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    this.hearts.callAll('scale.setTo', 'scale', 0.4, 0.4);
    game.time.events.loop(20000, this.addItem, this);

    //player bullet
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.createMultiple(60, 'star');
    bullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    bullets.callAll('scale.setTo', 'scale', 0.2, 0.2);
    bullets.setAll('checkWorldBounds', true);
    bullets.setAll('outOfBoundskill', true);

    //p2 bullet
    bullets2 = game.add.group();
    bullets2.enableBody = true;
    bullets2.createMultiple(60, 'star2');
    bullets2.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
   
    bullets2.setAll('checkWorldBounds', true);
    bullets2.setAll('outOfBoundskill', true);

    //enemybullet
    ebullets = game.add.group();
    ebullets.enableBody = true;
    ebullets.createMultiple(150, 'stone');
    ebullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    ebullets.callAll('scale.setTo', 'scale', 0.2, 0.2);
    ebullets.setAll('checkWorldBounds', true);
    ebullets.setAll('outOfBoundskill', true);

    //catbullet
    cbullets = game.add.group();
    cbullets.enableBody = true;
    cbullets.createMultiple(150, 'stone');
    cbullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
    cbullets.callAll('scale.setTo', 'scale', 0.2, 0.2);
    cbullets.setAll('checkWorldBounds', true);
    cbullets.setAll('outOfBoundskill', true);

    game.time.events.loop(3000, this.addRabbit, this);
    game.time.events.loop(10500, this.addGoat, this);
    game.time.events.loop(17200, this.addCat, this);
    game.time.events.loop(600, this.rabbit_attack, this);
    game.time.events.loop(1400, this.goat_attack, this);
    game.time.events.loop(3000, this.cat_attack, this);
    

    // PARTICLE
    this.eb_emitter = game.add.emitter(0, 0, 15);
    this.eb_emitter.makeParticles('pixel');
    this.eb_emitter.setYSpeed(-150, 150);
    this.eb_emitter.setXSpeed(-150, 150);
    this.eb_emitter.setScale(2, 0, 2, 0, 500);
    this.eb_emitter.gravity = 0;

    this.pb_emitters = [];
    
    var emitter;
    for (var e = 0; e < 10; e++){
      emitter = game.add.emitter(0, 0, 5);
      emitter.makeParticles('spark');
      emitter.setYSpeed(-150, 0);
      emitter.setXSpeed(-150, 150);
      emitter.setScale(2, 0, 2, 0, 500);
      emitter.gravity = 0;
      /* ... your emitter setup here ... */  
      this.pb_emitters.push(emitter);
    }
  },
  update: function() {
    bgtile.tilePosition.y += 1;
    // COLLISION
    game.physics.arcade.collide(this.rabbits, this.rightwall);
    game.physics.arcade.collide(this.rabbits, this.leftwall);
    game.physics.arcade.collide(this.goats, this.rightwall);
    game.physics.arcade.collide(this.goats, this.leftwall);
    game.physics.arcade.collide(this.cats, this.rightwall);
    game.physics.arcade.collide(this.cats, this.leftwall);
    game.physics.arcade.collide(this.walls, this.walls);
    game.physics.arcade.collide(this.player1, this.walls);
    game.physics.arcade.overlap(this.player1,this.rabbits, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player1,this.goats, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player1,this.cats, this.playerDie, null, this);
    game.physics.arcade.collide(this.player2, this.walls);
    game.physics.arcade.overlap(this.player2,this.rabbits, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player2,this.goats, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player2,this.cats, this.playerDie, null, this);

    game.physics.arcade.overlap(bullets, this.rabbits, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets, this.goats, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets, this.cats, this.enemyDie, null, this);
    game.physics.arcade.overlap(this.player1, ebullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player1, cbullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player1, this.hearts, this.takeHeart, null, this);

    game.physics.arcade.overlap(bullets2, this.rabbits, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets2, this.goats, this.enemyDie, null, this);
    game.physics.arcade.overlap(bullets2, this.cats, this.enemyDie, null, this);
    game.physics.arcade.overlap(this.player2, ebullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player2, cbullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player2, this.hearts, this.takeHeart, null, this);
    this.movePlayer();
    this.movePlayer2();
    if (!this.player1.inWorld) { this.playerDie();}
    if(p1_next_skill > game.time.now)
    {
      this.p1_skillLabel.addColor("#ffffff", 0);
    }
    else this.p1_skillLabel.addColor("#0072e3", 0);
    if(p2_next_skill > game.time.now)
    {
      this.p2_skillLabel.addColor("#ffffff", 0);
    }
    else this.p2_skillLabel.addColor("#28ff28", 0);
  
  }, // No changes
  movePlayer: function() {

    // LEFT RIGHT
    if (this.cursor.left.isDown) {
      this.player1.body.velocity.x = -200;
      this.player1.animations.play('leftwalk'); 
    }
    else if (this.cursor.right.isDown) {
      this.player1.body.velocity.x = 200;
      this.player1.animations.play('rightwalk'); 
    }
    else if (this.cursor.up.isDown) {
      this.player1.body.velocity.y = -200;
      this.player1.animations.play('forwardwalk'); 
    } 
    else if(this.cursor.down.isDown){
      this.player1.body.velocity.y = 200;
      this.player1.animations.play('forwardwalk'); 
    }
    else {
      this.player1.body.velocity.x = 0;
      this.player1.body.velocity.y = 0;
      this.player1.animations.play('forwardwalk'); 
    } 
      
    // shoot
    if(game.input.keyboard.isDown(Phaser.KeyCode.ENTER)){
      p1_fire(this.player1);
     // console.log("Enter!");
    }
    //skill
    if(game.input.keyboard.isDown(Phaser.Keyboard.ONE))
    {
      this.p1_use_skill();
    }
  },
  movePlayer2: function() {

    // LEFT RIGHT
    if (game.input.keyboard.isDown(Phaser.KeyCode.A)) {
      this.player2.body.velocity.x = -200;
      this.player2.animations.play('leftwalk'); 
    }
    else if (game.input.keyboard.isDown(Phaser.KeyCode.D)) {
      this.player2.body.velocity.x = 200;
      this.player2.animations.play('rightwalk'); 
    }
    else if (game.input.keyboard.isDown(Phaser.KeyCode.W)) {
      this.player2.body.velocity.y = -200;
      this.player2.animations.play('forwardwalk'); 
    } 
    else if(game.input.keyboard.isDown(Phaser.KeyCode.S)){
      this.player2.body.velocity.y = 200;
      this.player2.animations.play('forwardwalk'); 
    }
    else {
      this.player2.body.velocity.x = 0;
      this.player2.body.velocity.y = 0;
      this.player2.animations.play('forwardwalk'); 
    } 
      
    // shoot
    if(game.input.keyboard.isDown(32)){
      p2_fire(this.player2);
      //console.log("space!");
    }
    //skill
    if(game.input.keyboard.isDown(Phaser.Keyboard.TWO))
    {
      this.p2_use_skill();
    }
   
  },
  takeHeart: function(player, heart) {
    // SCORE
    if(player == this.player1 &&  game.global.player1Life<3)
        game.global.player1Life += 1;
    else if(player == this.player2 &&  game.global.player2Life<3)
        game.global.player2Life += 1;
    this.p1_lifeLabel.text = 'Life: ' + game.global.player1Life;
    this.p2_lifeLabel.text = 'Life: ' + game.global.player2Life;
    
    heart.kill();
   

    // TWEEN
    game.add.tween(player.scale).to({x: 0.15, y: 0.15}, 100).yoyo(true).start();
  },

  addItem: function(){
    var which = game.rnd.pick(['heart']);
    var posx = Math.floor((Math.random() * 350)+75);
    var posy = game.rnd.pick([350,360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500]);
    if(which == 'heart')
    {
      var heart = this.hearts.getFirstDead();
      if(!heart) return;
      heart.reset(posx, posy);
    }
  },
  addRabbit: function() {
   // console.log("addenemy1!");
    var rabbit = this.rabbits.getFirstDead();
    var posx = game.rnd.pick([100, 150, 200, 250, 300, 350, 400]);
    rabbit.scale.setTo(0.4, 0.4);
    if (!rabbit) return;
    rabbit.anchor.setTo(0.5, 1);
    rabbit.reset(posx, 100);
    rabbit.body.velocity.y = 40;
    rabbit.body.velocity.x = 50 * game.rnd.pick([-1, 1]) * (Math.random() +1);
    rabbit.body.bounce.x = 1;
    rabbit.checkWorldBounds = true;
    rabbit.outOfBoundsKill = true;
    rabbit.animations.add('walk', [1, 3], 8, true);
    rabbit.animations.play('walk'); 
    rabbit.health = 1;
  },
  addGoat: function() {
    //console.log("addgoat!");
    var goat = this.goats.getFirstDead();
    var posx = game.rnd.pick([100, 150, 200, 250, 300, 350, 400, 450])-30;
    goat.scale.setTo(0.4, 0.4);
    if (!goat) return;
    goat.alpha = 1.0;
    goat.anchor.setTo(0.5, 1);
    goat.reset(posx, 50);
    goat.body.velocity.y = 40;
    goat.body.velocity.x = 50 * game.rnd.pick([-1, 1]) * (Math.random() +1);
    goat.body.bounce.x = 1;
    goat.checkWorldBounds = true;
    goat.outOfBoundsKill = true;
    goat.animations.add('walk', [1, 3], 8, true);
    goat.animations.play('walk'); 
    goat.setHealth(2);
  },
  addCat: function() {
    //console.log("addgoat!");
    var cat = this.cats.getFirstDead();
    var posx = game.rnd.pick([50, 450])
    cat.scale.setTo(0.4, 0.4);
    if (!cat) return;
    cat.alpha = 1.0;
    cat.anchor.setTo(0.5, 1);
    cat.reset(posx, 200);
    cat.body.velocity.y = 20;
    cat.body.velocity.x = 50 * game.rnd.pick([-1, 1]) * (Math.random() +1);
    cat.body.bounce.x = 1;
    cat.checkWorldBounds = true;
    cat.outOfBoundsKill = true;
    cat.animations.add('walk', [1, 3], 8, true);
    cat.animations.play('walk'); 
    cat.setHealth(2);
    
  },
  playerDie: function(player, enemy) {
    this.hurt_sound.play();
    enemy.kill();
    //console.log("hurt");
    // PARTICLE SYSTEM
    this.eb_emitter.x = player.x;
    this.eb_emitter.y = player.y;
    this.eb_emitter.start(true, 800, null, 15);

    // CAMERA EFFECT
    //game.camera.flash(0xffffff, 300);
    //game.camera.shake(0.02, 300);

    if(player == this.player1)
        game.global.player1Life -= 1;
    else
        game.global.player2Life -= 1;
    this.p1_lifeLabel.text = 'Life: ' + game.global.player1Life;
    this.p2_lifeLabel.text = 'Life: ' + game.global.player2Life;
    if(game.global.player1Life <= 0)
    {
      p1_is_died = 1;
      this.player1.kill();
      if(p2_is_died == 1)
      {
        game.time.events.add(1000, function() {
            this.bgm.stop();
            game.state.start('over');
      
          }, this);
      }
      
    }
    if(game.global.player2Life <= 0)
    {
      p2_is_died = 1;
      this.player2.kill();
      if(p1_is_died == 1)
      {
        game.time.events.add(1000, function() {
            this.bgm.stop();
            game.state.start('over');
      
          }, this);
      }
      
    }
   
  },
  enemyDie: function(bullet, enemy){
    this.attack_sound.play();
    //console.log("enemyDie");
    currentExplosion = (currentExplosion + 1) % 10;
    var emitter = this.pb_emitters[currentExplosion];
    emitter.x = enemy.x;
    emitter.y = enemy.y;
    emitter.start(true, 500, null, 15);
    enemy.health -=1;
    if(enemy.health<=0)
      enemy.kill();
    else
      enemy.alpha = enemy.health/3;
    bullet.kill();
    game.global.score += 5;
    this.scoreLabel.text = 'score: ' + game.global.score;
    
  },
  rabbit_attack: function(){
    this.rabbits.forEachExists(function(enemy){
      
        var ebullet = this.ebullets.getFirstDead();
        if(!ebullet) return;
        ebullet.reset(enemy.x+ 17.75, enemy.y);
        //ebullet.body.velocity.y = enemy.body.velocity.y*5.0;
        ebullet.body.velocity.x = enemy.body.velocity.x*2.0;
        ebullet.checkWorldBounds = true;
        ebullet.outOfBoundsKill = true;
        ebullet.body.gravity.y = 500;
        //console.log("rabbit_attack");
    })
  },
  goat_attack: function(){
    this.goats.forEachExists(function(enemy){
        var attack_radius = 5;
        if(enemy.body.velocity.y != 0)
        {
          enemy.body.velocity.x = 0;
          enemy.body.velocity.y = 0;
          enemy.animations.stop();
          enemy.frame = 0
        }
        for(var i=0; i<6; i++)
        {
          var degree = i*(360/6)*Math.PI /180.0;
          var ebullet = this.ebullets.getFirstDead();
          if(!ebullet) return;
          
          ebullet.reset(enemy.x+ attack_radius * Math.cos(degree), enemy.y + attack_radius * Math.sin(degree));
          ebullet.body.velocity.y =  Math.sin(degree)*70.0;
          ebullet.body.velocity.x = Math.cos(degree)*70.0;
          ebullet.checkWorldBounds = true;
          ebullet.outOfBoundsKill = true;
          ebullet.body.gravity.y = 0;
        }
        
        //console.log("goat_attack");
    })
  },
  cat_attack: function(){
    this.cats.forEachExists(function(enemy){
        if(enemy.y > 400 || enemy.y < 100)
        {
          console.log(enemy.y);
          enemy.body.velocity.y *= -1;
        }
        for(var i=-1; i<2; i++)
        {
          var cbullet = this.cbullets.getFirstDead();
          if(!cbullet) return;
          
          cbullet.reset(enemy.x + 13, enemy.y +5);
          cbullet.body.velocity.y =  70.0;
          cbullet.body.velocity.x = i*50.0 + enemy.body.velocity.x;
          cbullet.checkWorldBounds = true;
          cbullet.outOfBoundsKill = true;
          cbullet.body.gravity.y = 0;
        }
        
        //console.log("cat_attack");
    })
  },
  p1_use_skill: function(){
    if(game.global.players1kill <= 0 || game.time.now < p1_next_skill || p1_is_died == 1 )return;
    else
    {
     p1_next_skill = game.time.now + p1_skillRate;
      game.global.player1skill -= 1;
      this.p1_skillLabel.text = 'Skill: ' + game.global.player1skill;
    }
    for(var i = 0; i < 2; i++)
    {
      for(var j=0; j<8; j++)
      {
        var bullet = bullets.getFirstDead();
        if(!bullet) return;
        bullet.scale.setTo(0.5, 0.5);
        bullet.reset(70 + j*50, this.player1.y+ 10 + 40*i);
        bullet.body.velocity.y = -600;
        //console.log("shoot!");
        bullet.outOfBoundsKill = true;  
      }
        
    }
  },
  p2_use_skill: function(){
    if(game.global.players2kill <= 0 || game.time.now < p2_next_skill || p2_is_died == 1) return;
    else
    {
      p2_next_skill = game.time.now + p2_skillRate;
      game.global.player2skill -= 1;
      this.p2_skillLabel.text = 'Skill: ' + game.global.player2skill;
    }
    for(var i = 0; i < 2; i++)
    {
      for(var j=0; j<8; j++)
      {
        var bullet = bullets2.getFirstDead();
        if(!bullet) return;
        //bullet.scale.setTo(0.5, 0.5);
        bullet.reset(50+i*400,200+j*50);
        bullet.body.velocity.y = -600;
        if(i==0) bullet.body.velocity.x = 500;
        else bullet.body.velocity.x = -500;
        //console.log("shoot!");
        bullet.outOfBoundsKill = true;  
      }
        
    }
  },
  pause: function(){
    if(is_pause == 0)
    {
      is_pause = 1;
      this.game.paused = true;
    }
    else{
      is_pause = 0;
      this.game.paused = false;
    }
  }
  
};
function p2_fire(player) {
  if(p2_is_died == 1) return;
  if (game.time.now > p2_nextFire && bullets.countDead() > 0)
  {
      p2_nextFire = game.time.now + p2_fireRate;
      for(var i=0; i<3; i++)
      {
        var bullet = bullets2.getFirstDead();
        if(!bullet) return;
        bullet.scale.setTo(0.7, 0.7);
        bullet.reset(player.x+ 11*i, player.y);
        bullet.body.velocity.y = -400;
       
        bullet.body.velocity.x = (i-1)*50.0 ;
        bullet.outOfBoundsKill = true;    
      }
      //console.log("shoot2!");
  }
};
function p1_fire(player) {
    if(p1_is_died == 1) return;
    if (game.time.now > nextFire && bullets.countDead() > 0)
    {
        nextFire = game.time.now + fireRate;
        var bullet = bullets.getFirstDead();
        bullet.scale.setTo(0.3, 0.3);
        bullet.reset(player.x+ 17.75, player.y);
        bullet.body.velocity.y = -400;
        //console.log("shoot!");
        bullet.outOfBoundsKill = true;    
    }
  };



