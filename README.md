# 106062326 Assignment_02 Raiden 
## page: https://106062326.gitlab.io/Assignment_02
## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Leaderboard|5%|Y|

## Basic Components Description :
1. Game process:
   - 分成4個state: boot->menu->play game->game over
   - 從game over state可以回到menu
2. Basic rules:
   - player:使用上下左右鍵移動、空白鍵發射、數字鍵1使用技能
   - player有三條生命，碰到敵人或敵人子彈會減一
   - Enemy: 有三種enemy
        1. rabbit: 一條生命，每次射出1顆子彈
        2. goat: 兩條生命，每次以圓形射出6顆子彈
        3. cat: 兩條生命，每次散射3顆子彈 
   - 有兩條生命的Enemy，第一次被擊中後會變透明，再被擊中一次才會消失
3. Jucify mechanisms:
   - Level:有3種level，越高級敵人生成頻率跟攻擊頻率較高，且level3有boss
   - skill:player1按下數字鍵1後，會一次射出兩排各10顆子彈，一場遊戲只能使用三次技能，技能有冷卻時間5秒
4. Animation:player和enemy有各自的動畫
5. Particle:player和enemy子彈擊中對方時會有粒子效果
6. UI:
   - 遊戲中左上角有血量及剩餘技能使用次數
   - 遊戲中正上方有目前分數
   - 遊戲中右上角有暫停按鈕，按下可暫停/恢復遊戲
   - 音量控制在menu正下方
7. Leaderboard:
    - 在menu中可選擇進入leaderboard
    - 以firebase實作，每次遊戲結束後輸入玩家名稱，並把分數與名稱一起push到database
    - 在leaderboard stste中，分不同關卡讀取database資料，並依照分數列出前三高分者

## Bonus Components
|Component|Y/N|
|:-:|:-:|
|Multi-player game(off-line)|Y|
|Enhanced items|Y|
|Boss|Y|

## Bonus Components Description :
1. Multi-player game(off-line):
   - 在menu中可選擇multi-player模式
   - player2的操作為wasd鍵移動、Enter發射、數字鍵2使用技能
   - player2的技能為從畫面兩側各自斜向射出8顆子彈
   - player2與player1相比，可以一次發射三顆子彈，但不能短時間內連續發射
   - 當player1、player2都死亡後，遊戲結束
2. Enhanced items:
   - 在遊戲中每過16秒鐘會隨機生成一種道具
   - heart:可回復一條生命
   - cherry: 在五秒鐘內讓子彈變成特殊子彈，大小較大，即使碰到敵人也不會消失，且可以消滅敵人的子彈
3. Boss:
   - 在level3中，過議定時間後會產生一隻boss，血量為30，會往玩家所在位置靠近，且有兩種攻擊模式(散射與圓形)
   - 消滅boss後可得分數50分，且遊戲強制結束
